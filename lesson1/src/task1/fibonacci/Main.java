package task1.fibonacci;

import java.util.Scanner;

/*
* Fibonacci series for GeekHub
* Java For Web
* Andrii Vasin andriivasin@gmail.com
* */

public class Main {

    public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	System.out.println("Calculating the Fibonacci sequence. Please enter the desired number");
    int n = scanner.nextInt();
    if (n >= 0){
        int first, second, next;
        first = 0;
        second = 1;

        System.out.println("Fibonacci series are: ");

        for (int i = 0; i <= n; i++) {
            System.out.println(first);
            next = first + second;
            first = second;
            second = next;
        }
    } else {
        System.out.println("Wrong input");
    }
    scanner.close();
    }
}
